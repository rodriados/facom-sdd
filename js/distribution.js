$(document).ready(function(){
	
	var svg = d3.select("svg")
	,   width = svg.attr("width")
	,   height = svg.attr("height")
	,   g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
	;
	
	var node = {'teacher': [], 'course': []}
	,   link = []
	,   info = {}
	,   elem = {}
	;
	
	d3.json("/data.json", function(data) {
		
		data.teachers.forEach(function (teacher, index, arr) {
			var coord = project(index + 1, arr.length, -1);
			
			node.teacher.push({
				'id': index
			,   'name': teacher.name
			,   'hours': teacher.hours
			,   'course': teacher.course
			,   'x': coord[0]
			,   'y': coord[1]
			});
			
			d3.select(".name-teacher select")
				.append("option")
				.text(teacher.name);
			
		});
		
		data.courses.forEach(function (course, index, arr) {
			var coord = project(index + 1, arr.length, 1);
			
			node.course.push({
				'id': index
			,   'name': course.name
			,   'hours': course.hours
			,   'teacher': course.teacher
			,   'required': course.required
			,   'x': coord[0]
			,   'y': coord[1]
			});
			
			d3.select(".name-course select")
				.append("option")
				.text(course.name);
			
		});
		
		node.teacher.forEach(function(teacher) {
			teacher.course.forEach(function(index) {
				link.push({
					'teacher': teacher
				,   'course': node.course[index]
				})
			});
		});
		
		g.append("text")
			.attr("transform", "translate(0,-300)")
			.style("text-anchor", "middle")
			.text(function() {
				return "Mostrando " + link.length + " distribuições."
			});
		
		info.teacher = d3.select(".name-teacher select .default")
			.text(node.teacher.length + " professores");
		
		info.course = d3.select(".name-course select .default")
			.text(node.course.length + " turmas");
		
		elem.link = g.selectAll(".link")
			.data(link).enter()
			.append("path")
			.attr("class", function(link) {
				return "link lt-" + link.teacher.id + " lc-" + link.course.id;
			})
			.attr("id", function(link) {
				return "l-" + link.teacher.id + "-" + link.course.id;
			})
			.attr("d", function(link) {
				return "M" + [link.course.x, link.course.y]
					 + "C" + [(link.course.x + link.teacher.x) / 2.0 + 50, link.course.y * 0.85]
					 + " " + [(link.course.x + link.teacher.x) / 2.0 - 50, link.teacher.y * 0.85]
					 + " " + [link.teacher.x, link.teacher.y]
				;
			})
			.on("mouseover", function(link) {
				$("#l-" + link.teacher.id + "-" + link.course.id + ", "
				+ "#t-" + link.teacher.id + ", "
				+ "#c-" + link.course.id)
					.each(function() {
						$(this).addClass("active");
					});
				
				info.teacher.text(link.teacher.name);
				info.course.text(link.course.name);
				
			})
			.on("mouseout", function(link) {
				$("#l-" + link.teacher.id + "-" + link.course.id + ", "
				+ "#t-" + link.teacher.id + ", "
				+ "#c-" + link.course.id)
					.each(function() {
						$(this).removeClass("active");
					});
				
				info.teacher.text(node.teacher.length + " professores");
				info.course.text(node.course.length + " turmas");
				
			});
		
		elem.course = g.selectAll(".node.course")
			.data(node.course).enter()
			.append("g")
			.attr("class", function(course) {
				var special = course.required
					? (course.teacher == null ? "required" : "okay")
					: "optional";
				return "node course " + special;
			})
			.attr("id", function(course) {
				return "c-" + course.id;
			})
			.attr("transform", function(course) {
				return "translate(" + [course.x, course.y] + ")";
			})
			.on("mouseover", function(course) {
				$("#c" + course.id + ", "
				+ ".lc-" + course.id)
					.each(function(){
						$(this).addClass("active");
					});
				
				info.course.text(course.name);
			})
			.on("mouseout", function(course) {
				$("#c" + course.id + ", "
					+ ".lc-" + course.id)
					.each(function(){
						$(this).removeClass("active");
					});
				
				info.course.text(node.course.length + " turmas");
			});
		
		elem.course.append("circle")
			.attr("r", 3.5);
		
		elem.course.append("text")
			.attr("dy", "0.31em")
			.attr("x", "10")
			.style("text-anchor", "start")
			.attr("transform", function(_, count) {
				return "rotate(" + rotate(count + 1, node.course.length, 1) + ")";
			})
			.text(function(course) {
				return course.name;
			});
		
		elem.teacher = g.selectAll(".node.teacher")
			.data(node.teacher).enter()
			.append("g")
			.attr("class", function(teacher) {
				var special = teacher.course.length < teacher.hours
					? "missing"
					: "okay";
				return "node teacher " + special;
			})
			.attr("id", function(teacher) {
				return "t-" + teacher.id;
			})
			.attr("transform", function(teacher) {
				return "translate(" + [teacher.x, teacher.y] + ")";
			})
			.on("mouseover", function(teacher) {
				$("#t" + teacher.id + ", "
				+ ".lt-" + teacher.id)
					.each(function(){
						$(this).addClass("active");
					});
				
				info.teacher.text(teacher.name);
				info.course.text(teacher.course.length + " turmas");
			})
			.on("mouseout", function(teacher) {
				$("#t" + teacher.id + ", "
				+ ".lt-" + teacher.id)
					.each(function(){
						$(this).removeClass("active");
					});
				
				info.teacher.text(node.teacher.length + " professores");
				info.course.text(node.course.length + " turmas");
			});
		
		elem.teacher.append("circle")
			.attr("r", 3.5);
	
		elem.teacher.append("text")
			.attr("dy", "0.31em")
			.attr("x", "-10")
			.style("text-anchor", "end")
			.attr("transform", function(_, count) {
				return "rotate(" + rotate(count + 1, node.teacher.length, -1) + ")";
			})
			.text(function(course) {
				return course.name;
			});
		
	});
});

function project(actual, total, mult) {
	
	var variation = 130 / (total + 1),
		angle = (actual * variation - 65) / 180 * Math.PI,
		radius = 300
		;
	
	return [mult * radius * Math.cos(angle), radius * Math.sin(angle)];
	
}

function rotate(actual, total, mult) {
	
	var variation = 130 / (total + 1);
	return actual * mult * variation + (-mult) * 65;
	
}
